package com.wangziyang.mes.system.config.swagger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.spring.web.DocumentationCache;
import springfox.documentation.spring.web.json.Json;
import springfox.documentation.spring.web.json.JsonSerializer;
import springfox.documentation.swagger2.mappers.ServiceModelToSwagger2Mapper;
import springfox.documentation.swagger2.web.Swagger2Controller;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@CrossOrigin(origins = {"http://localhost:9090", "null"})
@Controller
@ApiIgnore
public class Swagger2ControllerCORS extends Swagger2Controller {

    public static final String DEFAULT_URL = "/v2/api-docs";

    @Autowired
    public Swagger2ControllerCORS(Environment environment,
                                  DocumentationCache documentationCache,
                                  ServiceModelToSwagger2Mapper mapper,
                                  JsonSerializer jsonSerializer) {
        super(environment, documentationCache, mapper, jsonSerializer);

    }

    @RequestMapping(
            value = DEFAULT_URL,
            method = RequestMethod.GET,
            produces = { APPLICATION_JSON_VALUE })
    @Override
    @ResponseBody
    public ResponseEntity<Json> getDocumentation(String swaggerGroup, HttpServletRequest servletRequest) {
        return super.getDocumentation(swaggerGroup, servletRequest);
    }
}
